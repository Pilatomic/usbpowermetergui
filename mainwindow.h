#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>

QT_CHARTS_USE_NAMESPACE

class Device;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void updateDevicesList();
    void connectToDevice(int i);
    void processData(QByteArray b);
    void clearChart();

private:

    Ui::MainWindow *ui;
    Device* d;
    QChart *chart;
    QLineSeries* voltageSeries;
    QLineSeries* currentSeries;
    QDateTimeAxis *timeAxis;
    QValueAxis *voltageAxis;
    QValueAxis *currentAxis;
};
#endif // MAINWINDOW_H
