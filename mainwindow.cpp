#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "device.h"

#include <QChart>
#include <QChartView>
#include <QLineSeries>

QT_CHARTS_USE_NAMESPACE

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , d(new Device())
{
    ui->setupUi(this);

    connect(ui->scanButton, &QPushButton::clicked, d, &Device::startDeviceDiscovery);
    connect(ui->deviceComboBox, SIGNAL(activated(int)), this, SLOT(connectToDevice(int)));
    connect(ui->clearButton, &QPushButton::clicked, this, &MainWindow::clearChart);
    connect(d, &Device::devicesUpdated, this, &MainWindow::updateDevicesList);
    connect(d, &Device::dataReceived, this, &MainWindow::processData);

    d->startDeviceDiscovery();

    chart = new QChart();
    chart->setAnimationOptions(QChart::NoAnimation);
    chart->legend()->setVisible(false);

    voltageSeries = new QLineSeries;
    voltageSeries->setName("Voltage");
    voltageSeries->setColor(Qt::blue);

    currentSeries = new QLineSeries;
    currentSeries->setName("Current");
    currentSeries->setColor(Qt::red);

    timeAxis = new QDateTimeAxis();
    timeAxis->setFormat("HH:mm:ss");
    timeAxis->setTickCount(11);

    voltageAxis = new QValueAxis();
    voltageAxis->setTitleText("Voltage");
    voltageAxis->setTitleBrush(QBrush(voltageSeries->color()));
    voltageAxis->setTickCount(6);
    voltageAxis->setMinorTickCount(4);

    currentAxis = new QValueAxis();
    currentAxis->setTitleText("Current");
    currentAxis->setTitleBrush(QBrush(currentSeries->color()));
    currentAxis->setTickCount(6);
    currentAxis->setMinorTickCount(4);

    chart->addAxis(timeAxis, Qt::AlignBottom);
    chart->addAxis(voltageAxis, Qt::AlignLeft);
    chart->addAxis(currentAxis, Qt::AlignRight);

    chart->addSeries(voltageSeries);
    chart->addSeries(currentSeries);

    voltageSeries->attachAxis(voltageAxis);
    voltageSeries->attachAxis(timeAxis);

    currentSeries->attachAxis(currentAxis);
    currentSeries->attachAxis(timeAxis);

    clearChart();

    ui->chartView->setChart(chart);
    ui->chartView->setRenderHint(QPainter::Antialiasing);
    //ui->chartWidget->setMinimumSize(640, 480);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDevicesList()
{
    ui->deviceComboBox->clear();
    foreach(QBluetoothDeviceInfo di, d->getDevices())
    {
        ui->deviceComboBox->addItem(di.name(), di.address().toString());
    }
}

void MainWindow::connectToDevice(int i)
{
    d->scanServices(i);
}

void MainWindow::processData(QByteArray b)
{
    QString s;
    for(int i = 0 ; i < b.length() ; i++)
    {
        s.append(QString("%1 ").arg((int)((uint8_t)b.at(i)),2,16, QLatin1Char('0')).toUpper());
    }

    float voltage = ((float)((int)(((uint8_t)b.at(5) << 8) | (uint8_t)b.at(6)))) / 100;
    float current = ((float)(int)(((uint8_t)b.at(8) << 8) | (uint8_t)b.at(9))) / 100;
    float mAh = ((float)(int)(((uint8_t)b.at(0xA) << 16) | ((uint8_t)b.at(0xB) << 8) | (uint8_t)b.at(0xC)));
    float wH = ((float)(int)(((uint8_t)b.at(0xD) << 16) | ((uint8_t)b.at(0xE) << 16) |
                      ((uint8_t)b.at(0xF) << 8) | (uint8_t)b.at(0x10))) / 100;
    float vdp = ((float)(int)(((uint8_t)b.at(0x11) << 8) | (uint8_t)b.at(0x12))) / 100;
    float vdm = ((float)(int)(((uint8_t)b.at(0x13) << 8) | (uint8_t)b.at(0x14))) / 100;
    float temp = ((float)(int)(((uint8_t)b.at(0x15) << 8) | (uint8_t)b.at(0x16)));
    float hours = ((float)(int)(((uint8_t)b.at(0x17) << 8) | ((uint8_t)b.at(0x18))));
    float min = (uint8_t)b.at(0x19);
    float sec =  (uint8_t)b.at(0x1A);
    QTime time(hours, min, sec);

    QDateTime currDateTime = QDateTime::currentDateTime();
    qint64 x = currDateTime.toMSecsSinceEpoch();

    voltageSeries->append(x, voltage);
    currentSeries->append(x, current);

    if(voltage * 1.1 > voltageAxis->max()) voltageAxis->setMax(qCeil(voltage * 1.1 / 2.5) * 2.5);
    if(current * 1.1  > currentAxis->max()) currentAxis->setMax(qCeil(current * 1.1 / 0.5) * 0.5);
    if(currDateTime > timeAxis->max()) timeAxis->setMax(currDateTime);

    chart->update();

    ui->voltageDisplay->display((double)voltage);
    ui->currentDisplay->display(current);
    ui->powerDisplay->display(voltage * current);
    ui->capacityDisplay->display(mAh);
    ui->energyDisplay->display(wH);
    ui->vdpDisplay->display(vdp);
    ui->vdmDisplay->display(vdm);
    ui->tempDisplay->display(temp);


    //qDebug() << " : " << s;
    qDebug() << "V:" << voltage << ", I:" << current
             << ", mAh:" << mAh << ", Wh:" << wH
             << ", Vdp:" << vdp << ", Vdm:" << vdm
             << ", Temp:" << temp << ", Time:" << hours << min << sec;
}

void MainWindow::clearChart()
{
    voltageSeries->clear();
    currentSeries->clear();
    ui->chartView->update();
    timeAxis->setMin(QDateTime::currentDateTime());
    timeAxis->setMax(QDateTime::currentDateTime().addSecs(60));
    currentAxis->setMax(0.5);
    voltageAxis->setMax(7.5);
}

